import serial
import time
from matplotlib import pyplot as plt

ser = serial.Serial('/dev/ttyACM0',115200)

def listanalysis (inputlist):

    result = []

    maximum_y           = max(inputlist)
    maximum_x           = inputlist.index(maximum_y)

    leftthreshold_y     = 0.5*maximum_y
    leftthreshold_x     = 0

    rightthreshold_y    = 0.5*maximum_y
    rightthreshold_x    = 0

    for i in range(maximum_x,0, -1):
        if (inputlist[i] < leftthreshold_y):
            leftthreshold_x = i
            break

    for i in range(maximum_x, len(inputlist)-1):
        if (inputlist[i] < rightthreshold_y):
            rightthreshold_x = i
            break
    
    # list output format: [maximum_x,maximum_y,leftthreshold_x,leftthreshold_y,rightthreshold_x,rightthreshold_y]
    result.extend([maximum_x,maximum_y,leftthreshold_x,leftthreshold_y,rightthreshold_x,rightthreshold_y])

    return result

def graphanalysis (inputlist,color_):
    listpoints = listanalysis(inputlist)
    maxpoint = listpoints[0:2]
    leftthresholdpoint = listpoints[2:4]
    rightthresholdpoint = listpoints[4:6]

    plt.plot(maxpoint[0], maxpoint[1], marker='o', markersize=6, color=color_)
    plt.plot(leftthresholdpoint[0], leftthresholdpoint[1], marker='o', markersize=6, color=color_)
    
    if (rightthresholdpoint[0] != 0):
        plt.plot(rightthresholdpoint[0],rightthresholdpoint[1], marker='o', markersize=6, color=color_)

def dataanalysis (uplist,downlist,leftlist,rightlist):
    up_leftthres = int(listanalysis(uplist)[2])
    down_leftthres = int(listanalysis(downlist)[2])
    left_leftthres = int(listanalysis(leftlist)[2])
    right_leftthres = int(listanalysis(rightlist)[2])
    datalist = [up_leftthres,down_leftthres,left_leftthres,right_leftthres]
    if (max(datalist) == 0):
        print("Invalid data")
        return
    gesture = datalist.index(min(datalist))
    solution = ""
    if (gesture == 0):
        solution = "down"
    if (gesture == 1):
        solution = "up"
    if (gesture == 2):
        solution = "left"
    if (gesture == 3):
        solution = "right"
    print("Gesture: " + solution)

    #fig.suptitle("""matplotlib.figure.Figure.text()
    #function Example\n\n""",fontweight="bold")

start = 'Start Data Output\r\n'
end = "End Data Output"

while True:
    uplist      = []
    downlist    = []
    leftlist    = []
    rightlist   = []

    if (ser.readline() == start):
        print ("Start Communication")
        while True:
            data = ser.readline()
            x = data.split()
            if (x[0]=="End"):
                #print("break")
                break
            #print (int(x[0]),int(x[1]),int(x[2]),int(x[3]))
            uplist.append(int(x[0]))
            downlist.append(int(x[1]))
            leftlist.append(int(x[2]))
            rightlist.append(int(x[3]))
        print ("End Communication")

        plt.plot(uplist, color='red',label="down")
        plt.plot(downlist, color='green', label="up")
        plt.plot(leftlist, color='blue', label="left")
        plt.plot(rightlist, color='yellow', label="right")

        graphanalysis(uplist,"red")
        graphanalysis(downlist,"green")
        graphanalysis(leftlist, "blue")
        graphanalysis(rightlist, "yellow")

        dataanalysis(uplist,downlist,leftlist,rightlist)

        plt.xticks(range(0,len(uplist)+1, 10)) #set the tick frequency on x-axis
        plt.ylabel('register values') #set the label for y axis
        plt.xlabel('time') #set the label for x-axis
        plt.title("Gesture sensor time series") #set the title of the graph
        plt.legend()
        plt.show() #display the graph
        time.sleep(2)

# Value x: index value
# Value y: register value
