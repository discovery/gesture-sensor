/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_USART1_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

//FIFODATA_T input_buffer[32];
FIFODATA_T data_buffer[1024] = {0};

FIFODATA_T input_data;

bool ArrayThreshold (FIFODATA_T InputArray[], int len, int threshold){

	for (int i = 0; i < len; ++i){
		if (InputArray[i].UP >= threshold){
			return true;
		}
		if (InputArray[i].DOWN >= threshold){
			return true;
		}
		if (InputArray[i].LEFT>= threshold){
			return true;
		}
		if (InputArray[i].RIGHT >= threshold){
			return true;
		}
	}

	return false;
}

void UpdateFIFO (FIFODATA_T * FIFODATA){

    //printf("Reading FIFO!\r\n");
    	for(int i = 0; i < 1024; ++i){
    		FIFODATA[i].UP = read(APDS9960_GFIFO_U);
    		FIFODATA[i].DOWN =  read(APDS9960_GFIFO_D);
    		FIFODATA[i].LEFT = read(APDS9960_GFIFO_L);
    		FIFODATA[i].RIGHT = read(APDS9960_GFIFO_R);
    	}
}

void write(uint8_t Register, uint8_t Value){

	HAL_I2C_Mem_Write(&hi2c1, APDS9960_I2C_ADDR, Register, 1, &Value, 1, HAL_MAX_DELAY); //Power-ON && Gesture Enable

}

uint8_t read(uint8_t Register){

	uint8_t Value = 0;
	HAL_I2C_Mem_Read(&hi2c1, APDS9960_I2C_ADDR, Register, 1, &Value, 1, HAL_MAX_DELAY);
	return Value;
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  //bitfields for the engines
  uint8_t PON				= 0b00000001;
  uint8_t PEN				= 0b00000100;
  uint8_t GEN 				= 0b01000000;
  uint8_t WEN				= 0b00001000; //enables WTIME

  //setup of the Gesture Engine
  uint8_t GMODEOFF			= 0b00000000;
  uint8_t GDIMS_SET 		= 0b00000000;
  uint8_t GPENTH_DEFAULT	= 0b00001010; //Entry threshold for Gesture Engine via Proximity Engine, set to 10
  uint8_t GEXTH_DEFAULT		= 0b00001010; //Exit threshold for Gesture Engine, set to 10
  uint8_t GFIFOTH_DEFAULT	= 0b11000000;
  uint8_t LEDBOOST_DEFAULT	= 0b00110001; //300% LED-BOOST


  write(APDS9960_ENABLE,PON | PEN | GEN);
  write(APDS9960_GCONF2, GGAIN_8X);
  write(APDS9960_GCONF3, GDIMS_SET);
  write(APDS9960_GPENTH, GPENTH_DEFAULT);
  write(APDS9960_GEXTH, GEXTH_DEFAULT);
  write(APDS9960_GCONF4, GMODEOFF);
  write(APDS9960_GCONF1, GFIFOTH_DEFAULT);
  //write(APDS9960_CONFIG2, LEDBOOST_DEFAULT);
  //write(APDS9960_WTIME, DEFAULT_WTIME);



  //write(APDS9960_GOFFSET_U,DEFAULT_GOFFSET);
  //write(APDS9960_GOFFSET_D,DEFAULT_GOFFSET);
  //write(APDS9960_GOFFSET_L,DEFAULT_GOFFSET);
  //write(APDS9960_GOFFSET_R,DEFAULT_GOFFSET);

  printf("MCU started\r\n");

  while (1)
  {
	  int i = 0; //count data-size
	  int gesture_flag = 1;
	  while(read(APDS9960_GCONF4) && read(APDS9960_GSTATUS)){
		  if (gesture_flag){
			  gesture_flag = 0;
			  printf("Start Data Output\r\n");
		  }
		  //DO SOMETHING USEFUL
		  	  if (i > 1023){
		  		  gesture_flag = 2;
		  		  break;
		  	  }
			  uint8_t UP = read(APDS9960_GFIFO_U);
			  uint8_t DOWN =  read(APDS9960_GFIFO_D);
			  uint8_t LEFT = read(APDS9960_GFIFO_L);
			  uint8_t RIGHT = read(APDS9960_GFIFO_R);
			  printf("%i %i %i %i \r\n",UP,DOWN,LEFT,RIGHT);
			  data_buffer[i].UP = UP;
			  data_buffer[i].DOWN = DOWN;
			  data_buffer[i].LEFT = LEFT;
			  data_buffer[i].RIGHT = RIGHT;
			  ++i;
			  HAL_Delay(5);
//		  uint8_t GVALID = read(APDS9960_GSTATUS);
//		  printf("%i\r\n",GVALID);

	  }
	  if (!gesture_flag) printf("End Data Output\r\n");
	  else if (gesture_flag == 2) printf("Error in Data Output\r\n");
	  HAL_Delay(100);

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  /* USER CODE END 3 */
}
 return 0;
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 40;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x10909CEC;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOB_CLK_ENABLE();

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
