/*
 * apds9960_regs.h
 *
 *  Created on: Dec 1, 2021
 *      Author: alexander
 */

#ifndef INC_APDS9960_REGS_H_
#define INC_APDS9960_REGS_H_

#include "inttypes.h"

#define APDS9960_I2C_ADDR 0x39
#define APDS9960_I2C_TIMEOUT 100

/* Setup registers */
#define APDS9960_ENABLE_REG 0x80 /* Enable Register */

/* Gesture Configuration registers */

/* Calibration registers */

/* Data registers */
#define APDS9960_ID_REG 0x92 /* ID Register */

/* unsorted registers */






#endif /* INC_APDS9960_REGS_H_ */
