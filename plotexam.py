from matplotlib import pyplot as plt
import random

data = [random.randint(0, 20) for i in range(0, 10)]  #create a list of 10 random numbers
data1 = [random.randint(0,20) for i in range(0, 10)]

plt.plot(data, color='magenta', label='hallo') #plot the data
plt.plot(data1, color='red', label='swag')

plt.xticks(range(0,len(data)+1, 1)) #set the tick frequency on x-axis

plt.ylabel('register values') #set the label for y axis
plt.xlabel('time') #set the label for x-axis
plt.title("Gesture sensor time series") #set the title of the graph
plt.text(0,0,"Gesture: up")
plt.legend()
plt.show() #display the graph